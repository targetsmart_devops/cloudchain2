# cloudchain2

Glue for Amazon Secrets.

## Installation

Add `git+ssh://git@bitbucket.org/targetsmart_devops/cloudchain2` to your requirements.txt pip dependency file.

## Example


Assiming there is a AWS Secrets Manager vault named `prod/example` configured with this JSON secret value:

```
[
  {
    "Comment": "This is a description",
    "Secret": "the secret value",
    "Service": "some_service",
    "Username": "some_user"
  },
  {
    "Comment": "This is another description",
    "Secret": "some other secret value",
    "Service": "some_other_service",
    "Username": "some_other_user"
  }
]
```

Then use cloudchain2 to retrieve one of the secrets:

```
from cloudchain2 import Cloudchain
cloudchain = Cloudchain("prod/example")
secret = cloudchain.readcreds("some_service", "some_user")
```