from setuptools import setup

setup(
    name='cloudchain2',
    py_modules=['cloudchain2'],  # this must be the same as the name above
    version='0.1.4',
    description='Secure, easy secrets.',
    author='Jonathan Buys',
    author_email='jonathan.buys@targetsmart.com',
    url='https://bitbucket.org/targetsmart_devops/cloudchain2.git',
    download_url='https://bitbucket.org/targetsmart_devops/cloudchain2.git',
    keywords=['secrets', 'password', 'account'],
    classifiers=[],
    install_requires=[
        'boto3>=1.7.9'
    ]
)
