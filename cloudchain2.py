import json
import boto3
import os


class Cloudchain:
    """Wrapper class for AWS secretsmanager to provide an upgrade path
    for existing Cloudchain deployments.
    """

    def __init__(self, secret_name, region_name='us-east-1', bypass=False):
        """
        - `secret_name`: The AWS Secrets Manager secret name
        - `region_name`: AWS region
        - `bypass`: Set to True to not interact with AWS and instead read from environment variables.
        """
        self.raw_secret_value = None
        self.secret_name = secret_name
        self.region_name = region_name
        self.bypass = bypass

    def refresh(self):
        """Re-interact with AWS Secrets Manager to lookup the secret
        associated with `cloudchain_object.secret_name`. The
        associated secret is stored to
        `cloudchain_object.raw_secret_value`. See `get` or `readcreds`
        which both call `refresh` the initially.
        """
        if self.bypass:
            return

        session = boto3.session.Session()
        client = session.client('secretsmanager', region_name=self.region_name)
        resp = client.get_secret_value(SecretId=self.secret_name)
        self.raw_secret_value = resp['SecretString']

    def get(self):
        """Get the secret. If `cloudchain_object.bypass` then attempt to get
        the secret from environment variables instead (CLOUDCHAIN_{SECRET}).
        """
        if self.bypass:
            return os.environ.get('CLOUDCHAIN_{}'.format(self.secret_name))

        if self.raw_secret_value is None:
            self.refresh()

        return self.raw_secret_value

    def readcreds(self, service, username):
        """Return a secret string associated with `service` and `username`.
        This method is designed to replicate the functionality of
        Cloudchain 1. To use, it is assumed that a valid JSON list of
        secret objects has been stored as the AWS Secrets Manager
        secret associated with `cloudchain_object.secret_name` and
        each list entry has these keys:

        - Secret
        - Service
        - Username

        If `cloudchain_object.bypass` is True, then an attempt is made
        to retrieve the secret from an environment variable
        `CLOUDCHAIN_{SERVICE}_{USERNAME}` (all upper case).

        In bypass mode, no exception is raised if the credentials are
        not found. None is returned.

        If not bypass mode, a ValueError is raised if no entry was
        found for the service/username values provided. The search for
        a matching entry is case sensitive.

        """
        if self.bypass:
            # attempt to pull from environment variable instead
            key = 'CLOUDCHAIN_{}_{}'.format(service.upper(), username.upper())
            return os.environ.get(key, None)

        if self.raw_secret_value is None:
            self.refresh()

        secret_list = tuple(json.loads(self.raw_secret_value))

        matches = [x for x in secret_list if x['Username'] == username
                   and x['Service'] == service]

        if not matches:
            raise ValueError(
                "No secret found for service {} and username {}".format(
                    service, username))

        return matches[0]['Secret']
